# Game of Life at Arduino

**Application for the Arduino microcontroller, which controls the simulation of cellurar automation "Game of life". The device is based on three modules: ATMega microcontroller, monochromatic graphic screen Nokia 5110 and joystick. The application was created using language C, libraries: Arduino, Adafruit GFX, Timers and driver PCD8544.**
## Table of Contents
* [General info](#general-info)
* [Technologies and Modules](#technologies-and-modules)
* [Setup](#setup)
* [Features](#features)
* [Softwere manual](#softwere-manual)
* [Author](#author)

## General info
The effect of the simulation was displayed on the matrix of the graphic screen used in the Nokia 5110 phones and Nokia 3310. The user interacts with the application by joystick. The application was created using the programming language C and meets all the assumptions of the cellular automaton. Communication of the microcontroller with the screen is done through PCD8544 driver with built-in SPI protocol. For displaying graphic elements on the matrix, the Adafruit GFX library is responsible. The user has the option of loading a initial state, drawing his own version of board, changing settings and simulations. Navigation of the software stored in the EEPROM memory.

Microcontrollers have a small amount of RAM. The simulation forces the use of two large tables, whose size exceeds value of RAM of the microcontroller. In order to reduce the application's demand for RAM, bit operations were used. As a result of such operation, the program's demand for RAM memory has dropped by 8 times.

## Technologies and Modules
Project is created with:
* C
* Arduino
* Adafruit GFX
* Adafruit PCD8544
* Timers: 16.4.1

Requires modules:
*  Graphics display Nokia 5110 <br />
[Buy here](https://www.sparkfun.com/products/10168) <br />
![Algorithm schema](./Images/ekran_nokia_5110_przod.png) ![Algorithm schema](./Images/ekran_nokia_5110_tyl.png) 
* Joystick <br />
[Buy here](https://botland.com.pl/pl/joystick/4093-thumb-joystick-z-przyciskiem-v2-modul-z-plytka.html) <br />
![Algorithm schema](./Images/joystick.jpg) 
* Arduino Uno R3 <br />
[Buy here](https://store.arduino.cc/arduino-uno-rev3) <br />
![Algorithm schema](./Images/arduino_uno.png) 

## Setup
The easiest way to connect the Arduino to the display is to connect directly. According to the manufacturer's instructions for making such a connection, use the Arduino pin supplying 3.3 Volts. The direct connection also works under a voltage of 5 V. Ignoring the manufacturer's recommendation may lead to a shortened service life of the display
The joystick requires the use of two analog inputs, through which information about the position of the knob for each axis is transmitted. A joystick button has been connected to the digital pin. The position of the knob is determined by reading the voltage value. By pressing the button, the status of the digital pin will be forced to the low state

**The module connection diagram can be found below: <br />**
![Algorithm schema](./Images/podlaczenie_ekranu_joysticka_do_arduino.png)

## Features
Main menu: <br />
![Algorithm schema](./Images/menu.png) <br />
Pause nad state menu: <br />
![Algorithm schema](./Images/pause.png) <br />
Symulation of cellular automata: <br />
![Algorithm schema](./Images/automat.png) <br />
Menu of ready state board: <br />
![Algorithm schema](./Images/ready_state.png) <br />
Example of ready state board: <br />
![Algorithm schema](./Images/rec.png) ![Algorithm schema](./Images/line.png) <br />
Draw own board: <br />
![Algorithm schema](./Images/own.png) <br />
Menu of roles: <br />
![Algorithm schema](./Images/rules.png) <br />

## Softwere manual
[Download here in pdf](https://mega.nz/#!QA40QSzI!4Y0nt7VYFs_gOU4WYEoJwhcJEws7P21xP26fe9RRIkQ) <br />

## Author
Maricn Dec - student Poznan University of Technology <br />
marcindec@outlook.com