#include <Adafruit_GFX.h>
#include <Adafruit_PCD8544.h>
#include <avr/pgmspace.h>
#include <Timers.h>

#define PIN_SCLK 13
#define PIN_DIN 11
#define PIN_DC 5
#define PIN_SCE 3
#define PIN_RST 4
#define PIN_ANALOG_X 0
#define PIN_ANALOG_Y 1
#define PIN_SWITCH 1

#define readPixel(x,y) bitRead(plane[(x)][(y)/8],(y)%8)
#define setPixel(x,y) bitSet(plane[(x)][(y)/8],(y)%8)
#define clearPixel(x,y) bitClear(plane[(x)][(y)/8],(y)%8)
#define setPixel2(x,y) bitSet(plane2[(x)][(y)/8],(y)%8)
#define clearPixel2(x,y) bitClear(plane2[(x)][(y)/8],(y)%8)

#define MENU_ARRAY_SIZE 4
#define SUBMENUSD_ARRAY_SIZE 1
#define SUBMENUSG_ARRAY_SIZE 3
#define SUBMENULP_ARRAY_SIZE 5
#define SUBMENUP_ARRAY_SIZE 2

Timer countStopTime;
byte counterPresses = 0;
byte liveCell = 3;
byte surviveCell = 2;
unsigned int timeTick = 100;
unsigned int generation = 0;
boolean switchedButton = false;
boolean cancelButton = false;
byte setContrast = 50;
byte counterPositionX = 0;
byte counterPositionY = 0;
byte i = 0;

const char PROGMEM MENU1[] = {"START"};
const char PROGMEM MENU2[] = {"SETUP GAME"};
const char PROGMEM MENU3[] = {"LOAD PLANE"};
const char PROGMEM MENU4[] = {"SETUP DISPLAY"};
const char PROGMEM MENU5[] = {"CONTRAST"};
const char PROGMEM MENU6[] = {"LIVE CELL"};
const char PROGMEM MENU7[] = {"SURVIVE CELL"};
const char PROGMEM MENU8[] = {"SET TICK"};
const char PROGMEM MENU9[] = {"RANDOM"};
const char PROGMEM MENU10[] = {"OWN PLANE"};
const char PROGMEM MENU11[] = {"RECTANGLE"};
const char PROGMEM MENU12[] = {"RESUME"};
const char PROGMEM MENU13[] = {"SHOW PLANE"};
const char PROGMEM MENU14[] = {"4 CELLS"};
const char PROGMEM MENU15[] = {"LINE"};

const char* const menu[MENU_ARRAY_SIZE] PROGMEM = {MENU1, MENU2, MENU3, MENU4};
const char* const submenuSD[SUBMENUSD_ARRAY_SIZE] PROGMEM = {MENU5};
const char* const submenuSG[SUBMENUSG_ARRAY_SIZE] PROGMEM = {MENU6, MENU7, MENU8};
const char* const submenuLP[SUBMENULP_ARRAY_SIZE] PROGMEM = {MENU9, MENU10, MENU11, MENU14, MENU15};
const char* const submenuP[SUBMENUP_ARRAY_SIZE] PROGMEM = {MENU12, MENU13};

char buffer[20];

byte plane[84][6];
byte plane2[84][6];

Adafruit_PCD8544 display = Adafruit_PCD8544(PIN_SCLK, PIN_DIN, PIN_DC, PIN_SCE, PIN_RST);

void setup() {
  countStopTime.begin(3000);
  pinMode(9, OUTPUT);
  digitalWrite(9, HIGH); //dioda w stanie wysokim
  pinMode(PIN_SWITCH, INPUT); //przycisk
  Serial.begin(9600);
  display.begin(); //wlaczenie ekranu
  display.setContrast(setContrast);
  delay(2000);
  display.clearDisplay();
}

void loop() {
  if (!switchedButton) {
    drawMenu(counterPresses, menu, MENU_ARRAY_SIZE);
  }

  //OBSLUGA PIERWSZEJ OPCJI "START"
  if (!(digitalRead(PIN_SWITCH)) && counterPresses == 0) {
    switchedButton = true;
    counterPresses = 0;
    display.clearDisplay();
    if (clearPlane())
      generateState();
    while (switchedButton) {
      displayPlane();
      delay(timeTick);
      nextStep();
      generation++;
      if (!(digitalRead(PIN_SWITCH))) {
        while (true) {
          drawMenu(counterPresses, submenuP, SUBMENUP_ARRAY_SIZE);
          display.setCursor(0, 41);
          display.print("GENERATION:");
          display.setCursor(65, 41);
          display.print(generation);
          display.display();
          //delay(100);
          if (!(digitalRead(PIN_SWITCH)) && counterPresses == 0 ) {
            break;
          }
          else if (!(digitalRead(PIN_SWITCH)) && counterPresses == 1 ) {
            while (switchedButton) {
              displayPlane();
              cancelOption(PIN_ANALOG_X);
            }
          }
        }
      }
      cancelOption(PIN_ANALOG_X);
    }
  }
  //OBSLUGA TRZECIEJ OPCJI "GAME SETUP"
  else if (!(digitalRead(PIN_SWITCH)) && counterPresses == 1) {
    switchedButton = true;
    cancelButton = false;
    counterPresses = 0;
    display.clearDisplay();
    while (switchedButton) {
      drawMenu(counterPresses, submenuSG, SUBMENUSG_ARRAY_SIZE);
      cancelOption(PIN_ANALOG_X);

      //SUBMENU
      // OBLSUGA PIERWSZEGO SUBMENU "LIVE CELL"
      if (!(digitalRead(PIN_SWITCH)) && counterPresses == 0) {
        cancelButton = false;
        display.clearDisplay();
        while (!cancelButton) {
          display.clearDisplay();
          delay(150);
          changeRules(liveCell);
          cancelOption(PIN_SWITCH);
        }
      }
      // OBLSUGA DRUGIEGO SUBMENU "SURVIVE CELL"
      else if (!(digitalRead(PIN_SWITCH)) && counterPresses == 1) {
        cancelButton = false;
        display.clearDisplay();
        while (!cancelButton) {
          display.clearDisplay();
          delay(150);
          changeRules(surviveCell);
          cancelOption(PIN_SWITCH);
        }
      }
      // OBLSUGA DRUGIEGO SUBMENU "SET TICK"
      else if (!(digitalRead(PIN_SWITCH)) && counterPresses == 2) {
        cancelButton = false;
        display.clearDisplay();
        while (!cancelButton) {
          display.clearDisplay();
          delay(150);
          changeRules(timeTick);
          cancelOption(PIN_SWITCH);
        }
      }
    }
  }
  else if (!(digitalRead(PIN_SWITCH)) && counterPresses == 2) {
    switchedButton = true;
    cancelButton = false;
    counterPresses = 0;
    display.clearDisplay();
    while (switchedButton) {
      drawMenu(counterPresses, submenuLP, SUBMENULP_ARRAY_SIZE);
      cancelOption(PIN_ANALOG_X);

      //SUBMENU
      // OBLSUGA PIERWSZEGO SUBMENU "RANDOM"
      if (!(digitalRead(PIN_SWITCH)) && counterPresses == 0) {
        display.clearDisplay();
        generateState();
        infoSuccess();
      }
      // OBLSUGA DRUGIEGO SUBMENU "OWN PLANE"
      else if (!(digitalRead(PIN_SWITCH)) && counterPresses == 1) {
        display.clearDisplay();

        if (!clearPlane()) {
          for (byte x = 0; x < 84; x++) {
            for (byte y = 0; y < 48; y++) {
              clearPixel(x, y);
            }
          }
        }

        while (true) {
          if (analogRead(PIN_ANALOG_X) < 300 && counterPositionX < 82) { // w prawo
            counterPositionX++;
            countStopTime.restart();
          }
          else if (analogRead(PIN_ANALOG_X) > 700 && counterPositionX > 0) { //w lewo
            counterPositionX--;
            countStopTime.restart();
          }
          else if (analogRead(PIN_ANALOG_Y) > 700 && counterPositionY < 46) { //w dol
            counterPositionY++;
            countStopTime.restart();
          }
          else if (analogRead(PIN_ANALOG_Y) < 300 && counterPositionY > 0) { //w gora
            counterPositionY--;
            countStopTime.restart();
          }

          display.drawRect(counterPositionX, counterPositionY, 3, 3, BLACK);
          display.display();

          delay(120);

          if (!(digitalRead(PIN_SWITCH)) && readPixel(counterPositionX + 1, counterPositionY + 1) == 0) {
            setPixel(counterPositionX + 1, counterPositionY + 1);
            countStopTime.restart();
          }
          else if (!(digitalRead(PIN_SWITCH)) && readPixel(counterPositionX + 1, counterPositionY + 1) == 1) {
            clearPixel(counterPositionX + 1, counterPositionY + 1);
            countStopTime.restart();
          }
          displayPlane();
          if (countStopTime.available()) {
            display.clearDisplay();
            break;
          }
        }
      }
      // OBLSUGA DRUGIEGO SUBMENU "RECTANGLE"
      else if (!(digitalRead(PIN_SWITCH)) && counterPresses == 2) {
        display.clearDisplay();
        generateRectangle();
        infoSuccess();
      }
      else if (!(digitalRead(PIN_SWITCH)) && counterPresses == 3) {
        display.clearDisplay();
        generateSimpleConf();
        infoSuccess();
      }
      else if (!(digitalRead(PIN_SWITCH)) && counterPresses == 4) {
        display.clearDisplay();
        generateLine();
        infoSuccess();
      }
    }
  }
  //OBSLUGA DRUGIEJ OPCJI "SETUP DISPLAY"
  else if (!(digitalRead(PIN_SWITCH)) && counterPresses == 3) {
    switchedButton = true;
    counterPresses = 0;
    display.clearDisplay();
    while (switchedButton) {
      delay(150);
      drawMenu(counterPresses, submenuSD, SUBMENUSD_ARRAY_SIZE);
      cancelOption(PIN_ANALOG_X);

      //SUBMENU
      // OBLSUGA PIERWSZEGO SUBMENU "CONTRAST"
      if (!(digitalRead(PIN_SWITCH)) && counterPresses == 0) {
        cancelButton = false;
        display.clearDisplay();
        while (!cancelButton) {
          delay(150);
          changeContrast();
          cancelOption(PIN_SWITCH);
        }
      }
    }
  }
}

//WYSWIETLENIE MENU
void drawMenu(byte counterPresses, const char* const table[], byte sizeArray) {
  byte positionCursorY = 0;
  for (i; i < sizeArray; i++) {
    display.setCursor(0, positionCursorY);
    if (i == counterPresses)
      display.drawRect(0, positionCursorY, 84, 11, BLACK);
    strcpy_P(buffer, (char*)pgm_read_word(&(table[i])));
    display.setCursor(2, positionCursorY + 2); //
    display.print(buffer);
    positionCursorY = positionCursorY + 12;
  }
  display.display();
  if (counterPresses <= 3) {
    display.clearDisplay();
    i = 0;
  }
  else {
    display.clearDisplay();
    i = counterPresses - 3;
  }
  movingCursor(sizeArray);
}

//PORUSZANIE PO MENU
void movingCursor(int sizeArray) {
  if (analogRead(PIN_ANALOG_Y) > 700) {
    if (counterPresses < (sizeArray - 1))
      counterPresses++;
  }
  else if (analogRead(PIN_ANALOG_Y) < 300) {
    if (counterPresses > 0)
      counterPresses--;
  }
  delay(200);
}

//ZMIANA KONTRASTU EKRANU
void changeContrast() {
  if (analogRead(PIN_ANALOG_X) < 300)
    setContrast += 2;
  else if (analogRead(PIN_ANALOG_X) > 700)
    setContrast -= 2;
    
  display.clearDisplay();
  display.setContrast(setContrast);
  display.drawChar(4, 20, '<', BLACK, WHITE, 1);
  display.drawChar(74, 20, '>', BLACK, WHITE, 1);
  display.fillRect(13, 19, setContrast - 20, 9, BLACK);
  display.setCursor(37, 10); //37 poniewaz 42 - 5 (rozmiar litery)
  display.print(setContrast);
  display.display();
}

//ZMIANA ZASAD AUTOMATU
void changeRules(unsigned int variableToChange) {
  if (counterPresses == 0) {
    if (analogRead(PIN_ANALOG_X) < 300) {
      if (liveCell < 8)
        liveCell++;
    }
    else if (analogRead(PIN_ANALOG_X) > 700) {
      if (liveCell > 0)
        liveCell--;
    }
  }
  else if (counterPresses == 1) {
    if (analogRead(PIN_ANALOG_X) < 300) {
      if (surviveCell < 8)
        surviveCell++;
    }
    else if (analogRead(PIN_ANALOG_X) > 700) {
      if (surviveCell > 0)
        surviveCell--;
    }
  }
  else if (counterPresses == 2) {
    if (analogRead(PIN_ANALOG_X) < 300) {
      timeTick += 100;
    }
    else if (analogRead(PIN_ANALOG_X) > 700) {
      timeTick -= 100;
    }
  }
  display.drawChar(4, 20, '<', BLACK, WHITE, 1);
  display.drawChar(74, 20, '>', BLACK, WHITE, 1);
  display.setCursor(39, 20); //39 poniewaz 84 - 5 /2 (rozmiar litery)
  display.print(variableToChange);
  display.display();
}

//FUNKCJA WYSWIETLAJACA PODGLAD PLANSZY
void infoSuccess() {
  displayPlane();
  delay(2000);
  display.clearDisplay();
}

//WYJSCIE Z OPCJI
void cancelOption(byte pin) {
  if (!(digitalRead(pin))) {
    switchedButton = true;
    counterPresses = 0;
    display.clearDisplay();
    cancelButton = true;
  }
  if (analogRead(pin) > 700) {
    switchedButton = false;
    counterPresses = 0;
    display.clearDisplay();
    cancelButton = true;
  }
}

//LOSOWA PLANSZA
void generateState() {
  for (byte x = 0; x < 84; x++) {
    for (byte y = 0; y < 48; y++) {
      if (random(0, 2) == 1)
        setPixel(x, y);
      else
        clearPixel(x, y);
    }
  }
}

//SPRAWDZENIE CZY PLANSZA ISTNIEJE (MA CHICIAZ JEDEN PIKSEL)
bool clearPlane() {
  for (byte x = 0; x < 84; x++)
    for (byte y = 0; y < 48; y++)
      if (readPixel(x, y))
        return false;
  return true;
}

//WYSWIETLENIE PLANSZY
void displayPlane() {
  for (byte x = 0; x < 84; x++)
    for (byte y = 0; y < 48; y++)
      display.drawPixel(x, y, readPixel(x, y) ? BLACK : WHITE);
  display.display();
}

//OBLICZENIE KOLEJNEGO KROKU
void nextStep() {
  for (byte x = 0; x < 84; x++) {
    for (byte y = 0; y < 48; y++) {
      byte near = checkNear(x, y);
      if (near == liveCell)
        setPixel2(x, y);
      else if (near != surviveCell)
        clearPixel2(x, y);
    }
  }
  for (byte x = 0; x < 84; x++) {
    for (byte y = 0; y < 6; y++) {
      plane[x][y] = plane2[x][y];
    }
  }
}

//SPRAWDZENIE LICZBY SASIADOW
byte checkNear(byte x, byte y) {
  if (x == 0 || y == 0 || x == 83 || y == 47)
    return 0;
  else
    return readPixel(x - 1, y - 1) + readPixel(x, y - 1) + readPixel(x + 1, y - 1) +
           readPixel(x - 1, y)     +                           readPixel(x + 1, y) +
           readPixel(x - 1, y + 1) + readPixel(x, y + 1) + readPixel(x + 1, y + 1);
}

//FUNKCJA RYSUJACA PROSTOKAT
void generateRectangle() {
  for (byte x = 0; x < 84; x++)
    for (byte y = 0; y < 48; y++)
      if (x > 20 && x < 64 && y > 10 && y < 38)
        setPixel(x, y);
      else
        clearPixel(x, y);
}

//FUNCKJA RYSUJACA LINIE
void generateSimpleConf() {
  for (byte x = 0; x < 84; x++)
    for (byte y = 0; y < 48; y++)
      if (x >= 42 && x <= 44 && y == 24)
        setPixel(x, y);
      else if (x == 43 && y == 23)
        setPixel(x, y);
      else
        clearPixel(x, y);
}

void generateLine() {
  for (byte x = 0; x < 84; x++)
    for (byte y = 0; y < 48; y++)
      if (x >= 0 && x <= 83 && y == 24)
        setPixel(x, y);
      else
        clearPixel(x, y);
}
